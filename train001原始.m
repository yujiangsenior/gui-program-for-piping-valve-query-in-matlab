function varargout = train001(varargin)
% TRAIN001 MATLAB code for train001.fig
%      TRAIN001, by itself, creates a new TRAIN001 or raises the existing
%      singleton*.
%
%      H = TRAIN001 returns the handle to a new TRAIN001 or the handle to
%      the existing singleton*.
%
%      TRAIN001('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRAIN001.M with the given input arguments.
%
%      TRAIN001('Property','Value',...) creates a new TRAIN001 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before train001_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to train001_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help train001

% Last Modified by GUIDE v2.5 08-Apr-2017 23:24:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @train001_OpeningFcn, ...
                   'gui_OutputFcn',  @train001_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before train001 is made visible.
function train001_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to train001 (see VARARGIN)

% Choose default command line output for train001
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes train001 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = train001_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
global num;
global name1;
global A;
conn=database('���ſ�','','');
ping(conn);
cursor1=exec(conn,'select ���� from բ��');
cursor1=fetch(cursor1);
A=cursor1.data;
tbl1=tabulate(A);
name=tbl1(:,1);
num=tbl1(:,2);
%name=[5];
name1=strrep(name(:,1),'/','');
set(handles.popupmenu1,'string',name1)

% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
global num;
global A;
global selectedval;

global name2;
global seq;
% B1=[];

list1=get(handles.popupmenu1,'String');
val1=get(handles.popupmenu1,'Value');
selectedval=list1{val1};
set(handles.edit1,'string',selectedval);
[bool,index]=ismember(A,selectedval);
seq=find(index);

set(handles.edit1,'string',selectedval);
conn=database('���ſ�','','');
ping(conn);
cursor2=exec(conn,'select PN from բ��');
cursor2=fetch(cursor2);
B=cursor2.data;

% for k=1:s
%     B1(k)=B(seq(k),1);
% end
a=1;
b=50;
B1=B(a:b);
tbl2=tabulate(B1);
name2=tbl2(:,1);
set(handles.popupmenu2,'string',name2);

% hObject    handle to popupmenu1 (see GCBO)
% for i=1:a
%     s1=BC{i,1};
%     s2=strtrim(s1);
%     ptr=strncmp(s2,'%%C',3);
% %    qtr=strncmp(s1,'\U+2205',7);
%     BC{i,1}=strrep(s2,'\U+2205','��');
%     if ptr>0
%     BC{i,1}=strrep(s2,'%%C','��');
%     end
%     B(i,1)=regexp(BC(i,1),'(?<=\w+)\d+','match');
%     C(i,1)=str2num(B{i,1}{1});
% end
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
