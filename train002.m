function varargout = train002(varargin)
% TRAIN002 MATLAB code for train002.fig
%      TRAIN002, by itself, creates a new TRAIN002 or raises the existing
%      singleton*.
%
%      H = TRAIN002 returns the handle to a new TRAIN002 or the handle to
%      the existing singleton*.
%
%      TRAIN002('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRAIN002.M with the given input arguments.
%
%      TRAIN002('Property','Value',...) creates a new TRAIN002 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before train002_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to train002_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help train002

% Last Modified by GUIDE v2.5 09-Apr-2017 14:04:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @train002_OpeningFcn, ...
                   'gui_OutputFcn',  @train002_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before train002 is made visible.
function train002_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to train002 (see VARARGIN)

% Choose default command line output for train002
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes train002 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = train002_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
conn=database('���ݿ�','','');
ping(conn);
cursor1=exec(conn,'select ���� from բ��');
cursor1=fetch(cursor1);
A=cursor1.data;
tbl1=tabulate(A);
name=tbl1(:,1);
num=tbl1(:,2);
[bool,index]=ismember('/Z41H',A);
[bool2,index2]=ismember('/Z41H',name);


cursor2=exec(conn,'select PN from բ��');
cursor2=fetch(cursor2);
B=cursor2.data;
a=index;
b=index+num{index2,1}-1;
B1=B(a:b);
tbl2=tabulate(B1);
name2=tbl2(:,1);

set(handles.popupmenu1,'string',name2)

% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
