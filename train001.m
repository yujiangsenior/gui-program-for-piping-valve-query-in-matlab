function varargout = train001(varargin)
% TRAIN001 MATLAB code for train001.fig
%      TRAIN001, by itself, creates a new TRAIN001 or raises the existing
%      singleton*.
%
%      H = TRAIN001 returns the handle to a new TRAIN001 or the handle to
%      the existing singleton*.
%
%      TRAIN001('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRAIN001.M with the given input arguments.
%
%      TRAIN001('Property','Value',...) creates a new TRAIN001 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before train001_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to train001_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help train001

% Last Modified by GUIDE v2.5 13-Apr-2017 00:06:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @train001_OpeningFcn, ...
                   'gui_OutputFcn',  @train001_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before train001 is made visible.
function train001_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to train001 (see VARARGIN)

% Choose default command line output for train001
handles.output = hObject;
global v;
v{1}=' 安全阀';v{2}=' 电动蝶阀';v{3}=' 电动减压阀';v{4}=' 电动截止阀';v{5}=' 电动球阀';v{6}=' 电动闸阀';
v{7}=' 蝶阀';v{8}=' 减压阀';v{9}=' 截止阀';v{10}=' 球阀';v{11}=' 疏水阀';v{12}=' 闸阀';v{13}=' 止回阀';
%以上注意：每个阀门前都有一个空格！
% set(handles.edit1,'string','0');
% set(handles.edit2,'string','0');
% set(handles.popupmenu4,'string',v);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes train001 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = train001_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
global v;
set(handles.edit1,'string','0');
set(handles.edit2,'string','0');

% valves=('安全阀','电动蝶阀','电动减压阀','电动截止阀','电动球阀','电动闸阀','蝶阀','减压阀','截止阀','球阀','疏水阀','闸阀','止回阀');
% v{1}='安全阀';v{2}='电动蝶阀';v{3}='电动减压阀';v{4}='电动截止阀';v{5}='电动球阀';v{6}='电动闸阀';
% v{7}='蝶阀';v{8}='减压阀';v{9}='截止阀';v{10}='球阀';v{11}='疏水阀';v{12}='闸阀';v{13}='止回阀';
set(handles.popupmenu4,'string',v);


% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit1_Callback(hObject, eventdata, handles)

if (isempty(input))
    set(hObject,'String','0');
end

% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
global v;
global A;
global B1;

global index1;
t=get(handles.popupmenu1,'value');
set(handles.edit1,'string','0');%每次点击代号，重量和长度归0；
set(handles.edit2,'string','0');
% set(handles.edit1,'string',t);

% list1=get(handles.popupmenu1,'String');
% val1=get(handles.popupmenu1,'Value');
% selectedval=list1{val1};
% set(handles.edit1,'string',selectedval);%作用是取得popupmenu1中显示的值；
t0=get(handles.popupmenu4,'value');

tbl1=tabulate(A);%tabulate概率统计函数
name=tbl1(:,1);
num=tbl1(:,2);
[bool1,index1]=ismember(name{t},A);%判断某阀门类别在A中第一次出现的位置；
[bool2,index2]=ismember(name{t},name);

conn=database('阀门库','','');
ping(conn);%连接数据库
valve=v{t0};
p='select';
q=' from';%注意：from前有一个空格！
col=' PN';%注意：PN前有一个空格！
words=strcat(p,col,q,valve);
cursor2=exec(conn,words);%游标

cursor2=fetch(cursor2);
B=cursor2.data;%B为所有PN值
a=index1;%得到起始位置
b=a+num{index2,1}-1;%得到终点位置
B1=B(a:b);
% for i=1:num{index2,1}
%     B2{i,1}=num2str(B1{i,1});
% end  

tbl2=tabulate(B1);%tabulate概率统计函数
name2=tbl2(:,1);

set(handles.popupmenu2,'value',1);
set(handles.popupmenu2,'string',name2);%将某阀门类别下的所有可能PN显示在第二个下拉窗口中；
close(cursor2);
close(conn);
% hObject    handle to popupmenu1 (see GCBO)

% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
global v;
global B1;

global index1;
global index3;

set(handles.edit1,'string','0');
set(handles.edit2,'string','0');

t=get(handles.popupmenu2,'value');
% set(handles.edit1,'string',t);

% conn=database('数据库','','');%待改成阀门库
% ping(conn);
% cursor2=exec(conn,'select PN from 闸阀');
% cursor2=fetch(cursor2);
% B=cursor2.data;

tbl2=tabulate(B1);
name2=tbl2(:,1);
num2=tbl2(:,2);
% num=tbl1(:,2);
[bool3,index3]=ismember(name2{t},B1);
[bool4,index4]=ismember(name2{t},name2);

t0=get(handles.popupmenu4,'value');
% v{1}=' 安全阀';v{2}=' 电动蝶阀';v{3}=' 电动减压阀';v{4}=' 电动截止阀';v{5}=' 电动球阀';v{6}=' 电动闸阀';
% v{7}=' 蝶阀';v{8}=' 减压阀';v{9}=' 截止阀';v{10}=' 球阀';v{11}=' 疏水阀';v{12}=' 闸阀';v{13}=' 止回阀';
%以上注意：每个阀门前都有一个空格！
conn=database('阀门库','','');
ping(conn);%连接数据库
valve=v{t0};
col=' DN';%注意：DN前有一个空格！
p='select';
q=' from';%注意：from前有一个空格！
words=strcat(p,col,q,valve);
cursor3=exec(conn,words);%游标

% cursor3=exec(conn,'select DN from 闸阀');
cursor3=fetch(cursor3);
C=cursor3.data;%C为所有DN值
a=index1+index3-1;%得到起始位置
b=a+num2{index4,1}-1;%得到终点位置
C1=C(a:b);
for i=1:num2{index4,1}
    C2{i,1}=num2str(C1{i,1});
end  %for循环为了将数据格式转换成字符串格式，不然tabulate不支持
tbl3=tabulate(C2);
name3=tbl3(:,1);

set(handles.popupmenu3,'value',1);
set(handles.popupmenu3,'string',name3);
%将某阀门类别下的某压力等级下所有可能DN值显示在第二个下拉窗口中；
close(cursor3);
close(conn);

% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)

set(handles.edit1,'string','0');
set(handles.edit2,'string','0');

% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
global v;
global index1;
global index3;

t=get(handles.popupmenu3,'value');
% set(handles.edit1,'string',t);

t0=get(handles.popupmenu4,'value');

conn=database('阀门库','','');
ping(conn);
valve=v{t0};
col=' 单重kg';%注意：单重kg前有一个空格！
p='select';
q=' from';%注意：from前有一个空格！
words=strcat(p,col,q,valve);
cursor4=exec(conn,words);

% cursor4=exec(conn,'select 单重kg from 闸阀');
cursor4=fetch(cursor4);
D=cursor4.data;%D为所有重量值
loc=index1+index3+t-2;%得到所选最终阀门的位置
weight=D{loc,1};%得到阀门重量

if strcmp(valve,' 安全阀')
col2=' 进口端至中心距离';%注意：前有一个空格！
words=strcat(p,col2,q,valve);
cursor6=exec(conn,words);
cursor6=fetch(cursor6);
F=cursor6.data;%E为所有长度值
length=F{loc,1};%得到阀门长度
set(handles.edit2,'string',length);%输出阀门前半长长度
col3=' 出口端至中心距离';%注意：前有一个空格！
words=strcat(p,col3,q,valve);
cursor7=exec(conn,words);
cursor7=fetch(cursor7);
G=cursor7.data;%E为所有长度值
length=G{loc,1};%得到阀门长度
set(handles.edit4,'string',length);%输出阀门后半长长度
close(cursor6);
close(cursor7);
else
col1=' 全长';%注意：全长前有一个空格！
words=strcat(p,col1,q,valve);
cursor5=exec(conn,words);
% cursor5=exec(conn,'select 全长 from 闸阀');
cursor5=fetch(cursor5);
E=cursor5.data;%E为所有长度值
length=E{loc,1};%得到阀门长度
set(handles.edit2,'string',length);%输出阀门长度
close(cursor5);
end

set(handles.edit1,'string',weight);%输出阀门重量


close(cursor4);

close(conn);

% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
global v;
global A;
set(handles.edit1,'string','0');
set(handles.edit2,'string','0');
t0=get(handles.popupmenu4,'value');

conn=database('阀门库','','');
ping(conn);%连接数据库
valve=v{t0};
col=' 名称';%注意：名称前有一个空格！
p='select';
q=' from';%注意：from前有一个空格！
words=strcat(p,col,q,valve);
cursor1=exec(conn,words);%游标
% cursor1=exec(conn,'select 名称 from 闸阀');


cursor1=fetch(cursor1);
A=cursor1.data;
tbl1=tabulate(A);%tabulate概率统计函数
name=tbl1(:,1);

name1=strrep(name(:,1),'/','');
set(handles.popupmenu1,'value',1);
set(handles.popupmenu1,'string',name1); %将所有阀门代号类别显示在第一个下拉窗口中；
close(cursor1);
close(conn);

% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
